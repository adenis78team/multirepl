package main

//cat viewobject.php | col -b > viewobject.php | cp viewobject.ph viewobject.php
import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

// cmd := exec.Command("find", "/", "-maxdepth", "1", "-exec", "wc", "-c", "{}", "\\")
// output, err := cmd.CombinedOutput()
// if err != nil {
//     fmt.Println(fmt.Sprint(err) + ": " + string(output))
//     return
// }
// fmt.Println(string(output))

//$(BUILD_DIR_NAME)
func main() {
	// if len(os.Args) < 4 {
	// 	fmt.Println("All three args is required: multirepl <old_replaced_string> <new_string> <file_extension>")
	// 	fmt.Print("but now there are just %d args: ", len(os.Args), os.Args)
	// 	return
	// }

	var (
		oldStr  = flag.String("old", "absolutelynothing", "the old line that needs to be replaced")
		newStr  = flag.String("new", "absolutelynothing", "the new line to replace the old one")
		fileext = flag.String("ext", "absolutelynoextension", "the extension of files to be proceed")
		proc    = flag.String("test", "no", "the extension of files to be proceed")
	)

	flag.Parse()
	fmt.Printf("oldStr=\"%s\", newStr=\"%s\", fileext=\"%s\"\n", *oldStr, *newStr, *fileext)

	if flag.Parsed() == false {
		flag.CommandLine.PrintDefaults()
		return
	}
	if *oldStr == "absolutelynothing" || *newStr == "absolutelynothing" || *fileext == "absolutelynoextension" {
		fmt.Println("All three args is required: multirepl <old_replaced_string> <new_string> <file_extension>")
		return
	}
	if *proc == "no" {
		fmt.Println("...just show files, do not proceed (test mode)")
	}
	smthfound := 0
	err := filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		fileext_len := len(*fileext) // длина расширения файла
		ix := strings.LastIndex(path, "/")
		if ix > -1 {
			// если это файл, а не каталог
			if info, err := os.Stat(path); err == nil && !info.IsDir() {
				filenameStr := path[ix+1:]
				if filenameStr != os.Args[0] {
					// если это файл c заданным расширением
					if len(filenameStr) >= fileext_len {
						if filenameStr[len(filenameStr)-fileext_len:] == *fileext {
							//fmt.Print("scan:" + filenameStr + "\n")

							dat, err := ioutil.ReadFile(path)
							check(err)
							s := string(dat)
							if strings.Contains(s, *oldStr) {
								smthfound++

								fmt.Print(path + "/" + filenameStr + "\n")
								if *proc == "yes" {
									result := strings.Replace(s, *oldStr, *newStr, -1)
									err = ioutil.WriteFile(path, []byte(result), 0)
									check(err)
								}
								//fmt.Print(s)
							}
						}
					}
				}
			}
		}
		return nil

		return nil
	})
	if err != nil {
		log.Println(err)
	}
	if smthfound == 0 {
		fmt.Println("Nothing found. Finished")
	} else {
		if *proc == "yes" {
			fmt.Printf("%d changes has done\n", smthfound)
		} else {
			fmt.Printf("%d changes could be made\n", smthfound)

		}
	}

	return

}
